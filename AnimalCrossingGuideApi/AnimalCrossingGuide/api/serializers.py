from AnimalCrossingGuide.api.models import Animal, Location, Category, ShadowSize
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['id', 'name']


class ShadowSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShadowSize
        fields = ['name', 'value']


class AnimalSerializer(serializers.ModelSerializer):
    shadowSize = serializers.SlugRelatedField(
        slug_field="name",
        queryset=ShadowSize.objects.all()
    )

    location = serializers.SlugRelatedField(
        slug_field="name",
        queryset=Location.objects.all()
    )

    category = serializers.SlugRelatedField(
        slug_field="name",
        queryset=Category.objects.all()
    )

    class Meta:
        model = Animal
        fields = '__all__'






