from AnimalCrossingGuide.api.models import Animal, Location, Category, ShadowSize
from rest_framework import viewsets, permissions
from AnimalCrossingGuide.api.serializers import CategorySerializer, LocationSerializer, AnimalSerializer, ShadowSizeSerializer
from rest_framework_api_key.permissions import HasAPIKey


class ShadowSizeViewSet(viewsets.ModelViewSet):
    """
    All rights for ADMIN only
    """
    permission_classes = [permissions.IsAdminUser]

    queryset = ShadowSize.objects.all()
    serializer_class = ShadowSizeSerializer


class LocationViewSet(viewsets.ModelViewSet):
    """
    All rights for ADMIN only
    """
    permission_classes = [permissions.IsAdminUser]

    queryset = Location.objects.all()
    serializer_class = LocationSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    """
        All rights for ADMIN only
    """
    permission_classes = [permissions.IsAdminUser]

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class AnimalViewSet(viewsets.ModelViewSet):
    """
    All rights for ADMIN, List and Retrieve for Authenticated Users
    """

    serializer_class = AnimalSerializer

    def get_permissions(self):
        if self.action == 'list' or self.action == 'retrieve':
            permission_classes = [permissions.IsAuthenticated | HasAPIKey]
        else:
            permission_classes = [permissions.IsAdminUser]
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        queryset = Animal.objects.all().order_by('name')
        category = self.request.query_params.get('category', None)
        if category is not None:
            queryset = queryset.filter(category=category)
        return queryset
